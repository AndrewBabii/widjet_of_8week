import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  Future<void> _showMainDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context){
        return AlertDialog(
          title: Text('Awesome dialog'),
          content: Text('ЗДЕСЬ МОЖЕТ БЫТЬ ВАША РЕКЛАМА'),
          actions: [
            FlatButton(onPressed:(){ Navigator.of(context).pop();}, child: Text('ok'))
          ],
        );
      }
    );
  }

  Future<void> _showSheet() async {
    return showCupertinoModalPopup(context: context, builder: (BuildContext context){
     return CupertinoActionSheet(
        title: Text('Awesome sheet'),
        message: Text('cupertino action'),
        actions: [
          CupertinoActionSheetAction(onPressed: (){Navigator.of(context).pop();}, child: Text('option'),),
          CupertinoActionSheetAction(onPressed: (){Navigator.of(context).pop();}, child: Text('something'),),
        ],
        cancelButton: CupertinoActionSheetAction(onPressed: (){Navigator.of(context).pop();}, child: Text('dismiss'),),
      );
    });
  }

  void _showSnackBar(){

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
CircularProgressIndicator(

),
            CupertinoActivityIndicator(
radius: 50,//it's optional
            ),

          ],
        ),
      ),
      floatingActionButton: Builder(
        builder: (context) => FloatingActionButton(
          onPressed: (){Scaffold.of(context).showSnackBar(SnackBar(content: Text('Awesome SnackBar'),));},
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ),
      ),

    );
  }
}
